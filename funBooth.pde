import gab.opencv.*;
import processing.video.*;
import java.awt.*;

Capture video;
OpenCV opencv;
PImage face;
import gohai.simpletweet.*;
SimpleTweet simpletweet;

void setup() {
  simpletweet = new SimpleTweet(this);
  simpletweet.setOAuthConsumerKey("");
  simpletweet.setOAuthConsumerSecret("");
  simpletweet.setOAuthAccessToken("");
  simpletweet.setOAuthAccessTokenSecret("");

  size(800, 600);
  video = new Capture(this, 640/2, 480/2);
  opencv = new OpenCV(this, 640/2, 480/2);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);  
  background(0);
  video.start();
}

void draw() {
  scale(2);

  opencv.loadImage(video);
  Rectangle[] faces = opencv.detect();
  for (int i = 0; i < faces.length; i++) {
    // image(video, 100, 0, 200, 100);
    PImage img = video.get(faces[i].x, faces[i].y, faces[i].width/2, faces[i].height);
    img.resize(0, 100);
    int x= int(random(4));
    int y=int(random(3));
    switch ((x+1)*(y+1)) {
    case 1:
      img.filter(DILATE);
      break;
    case 2:
      img.filter(POSTERIZE, 4);
      break;
    case 3:
      img.filter(GRAY);
      break;
    case 4:
      img.filter(INVERT);
      break;
    case 5:
      img.filter(POSTERIZE, 4);
      break;
    case 6:
      img.filter(POSTERIZE, 8);
      break;
    case 7:
      img.filter(ERODE);
      break;
    case 8:
      img.filter(POSTERIZE, 4);
      break;
    case 9:
      img.filter(BLUR, 4);
      break;
    case 10:
      img.filter(POSTERIZE, 4);
      break;
    case 11:
      img.filter(INVERT);
      break;
    case 12:
      img.filter(POSTERIZE, 4);
      break;
    }
    image(img, x*100, y*100);
    x= int(random(4));
    y= int(random(3));
      img = video.get(faces[i].x, faces[i].y, faces[i].width/2, faces[i].height);
    img.resize(0, 100);scale(-1, 1); 
    switch ((x+1)*(y+1)) {
    case 1:
      img.filter(DILATE);
      break;
    case 2:
      img.filter(POSTERIZE, 4);
      break;
    case 3:
      img.filter(GRAY);
      break;
    case 4:
      img.filter(INVERT);
      break;
    case 5:
      img.filter(POSTERIZE, 4);
      break;
    case 6:
      img.filter(POSTERIZE, 8);
      break;
    case 7:
      img.filter(ERODE);
      break;
    case 8:
      img.filter(POSTERIZE, 4);
      break;
    case 9:
      img.filter(BLUR, 4);
      break;
    case 10:
      img.filter(POSTERIZE, 4);
      break;
    case 11:
      img.filter(INVERT);
      break;
    case 12:
      img.filter(POSTERIZE, 4);
      break;
    }
    image(img, 50-width/2+(400-x*100)-148, y*100);
  }
}

void captureEvent(Capture c) {
  c.read();
}
void keyPressed() {
  if (key=='T') {
    String tweet = simpletweet.tweetImage(get(), "Made with Processing for @eartsup workshops ! #funfair #eartfunfair.");
    println("Posted " + tweet);
  }
}